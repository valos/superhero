"use strict";
/* jshint esversion: 6 */
/* jshint node: true */

const gulp = require('gulp');

const workbox = require('workbox-build');

const dist = 'app';

gulp.task('build', () => {
});

gulp.task('clean', () => {
});

gulp.task('generate-service-worker', () => {
    return workbox.generateSW({
        cacheId: 'superhero',
        globDirectory: dist,
        globPatterns: ['**\/*.{css,html,ico,js,json,png,jpg,woff2}'],
        swDest: `${dist}/sw.js`,
        importScripts: [],
        importWorkboxFrom: 'local',
        clientsClaim: true,
        skipWaiting: true
    }).then(() => {
        console.info('Service worker generation completed.');
    }).catch((error) => {
        console.warn('Service worker generation failed: ' + error);
    });
});
