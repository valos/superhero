# Superhero

A mini-encyclopedia of Superheroes and Villians from all universes

## Usage

### 1. Download this repository
```
git clone https://gitlab.com/valos/superhero
```

### 2. Install Yarn

As root user, run:
```
npm install yarn -g
```

### 3. Install dependencies

Go to the downloaded repository folder and run:
```
yarn install
```

### 4. Build superhereos data

```
./bin/superherodb.py
```

Data are scraped from [SHDb](http://www.superherodb.com/).

### 5. Generate service worker

```
gulp generate-service-worker
```

### 6. Run the app

```
yarn run serve
```

App will be opened in browser at `http://localhost:8081/`

## Live Preview

https://superhero.febvre.info


## Copyright

&copy; 2018 Valéry Febvre, All Rights Reserved
