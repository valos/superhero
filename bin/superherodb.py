#! /usr/bin/env python3

'''Scrapes the superherodb.com database'''

# max ID below 10000 is 1542
# 10000 MUST be avoid, it's a fake

import urllib.request
from bs4 import BeautifulSoup
import os
import re
import json
from PIL import Image

output_dir = 'app/data'
output_images_dir = os.path.join(output_dir, 'images')
output = os.path.join(output_dir, 'superhereos.json')

if not os.path.exists(output_dir):
    os.mkdir(output_dir)

if not os.path.exists(output_images_dir):
    os.mkdir(output_images_dir)

if os.path.exists(output):
    with open(output, 'r') as fp:
        hereos = json.load(fp)

    last_id = max([int(k) for k in hereos.keys()])
else:
    hereos = {}
    last_id = 0

max_id = last_id + 1075

stats_keys = ['intelligence', 'strength', 'speed', 'durability', 'power', 'combat']
biography_keys = ['full_name', 'alter_egos', 'aliases', 'place_of_birth', 'first_appearance', 'creator', 'alignment']
appearance_keys = ['gender', 'race', 'height', 'weight', 'eye_color', 'hair_color', 'skin_color']
work_keys = ['occupation', 'base']
connections_keys = ['teams', 'relatives']

for id in range(1, max_id + 1):
    image_name = '{0}.jpg'.format(id)
    image_path = os.path.join(output_images_dir, image_name)

    if (str(id) in hereos and os.path.exists(image_path)) or (id < last_id and str(id) not in hereos):
        continue

    url = 'http://www.superherodb.com/characters/10-' + str(id) + '/' #characters/10- n to 1384
    response = urllib.request.urlopen(url)
    data = response.read()
    soup = BeautifulSoup(data, 'html.parser')

    hero = dict(
        id=id,
        name=None,
        image=None,
        strength_level=dict(
            value=None,
            unit=None,
        ),
        powerstats={},
        biography={},
        appearance={},
        work={},
        connections={},
        powers=[],
    )

    name_el = soup.find_all('div', class_='titleprofile')[0].find_all('h1')[0]
    name = name_el.text.strip()

    image_el = soup.find('div', class_='cblock profileportrait')
    image = image_el.img['data-src'].split('?')[0]

    powerstats_els = soup.find('div', class_='col-md-7') \
        .find_all('div', class_='cblock')[1] \
        .find_all('div', class_="gridbarholder")

    # Skip if no name, no powerstats or no image
    if not name or not powerstats_els or not image or image.endswith('no-portrait.jpg'):
        print('SKIPPED', id, name)
        continue

    # main block: Biography, Appearence, Work, Connections
    main_block = soup.find('div', class_='col-md-7').find('div', class_='cblock linkunderline')

    biography_els = main_block.find_all('table')[0].find_all('tr')
    appearance_els = main_block.find_all('table')[1].find_all('tr')
    work_els = main_block.find_all('table')[2].find_all('tr')
    connections_els = main_block.find_all('table')[3].find_all('tr')

    # Name
    hero['name'] = name

    # Powerstats
    # for i, el in enumerate(powerstats_els):
    #     hero['powerstats'][stats_keys[i]] = el.find('div', 'gridbarvalue').text

    # Biography
    for i, el in enumerate(biography_els):
        key = biography_keys[i]
        value = el.find_all('td')[1].text.strip()
        if key == 'alter_egos' and value == 'No alter egos found.':
            value = None
        elif value == '-':
            value = None
        hero['biography'][key] = value

    # Appearance
    for i, el in enumerate(appearance_els):
        key = appearance_keys[i]
        value = el.find_all('td')[1].text.strip()
        if key == 'height' or key == 'weight':
            # Remove imperial (keep metric)
            value = value.split('//')[1].strip()
            value, unit = value.split()
            if value != "0":
                value = dict(
                    value=value,
                    unit=unit,
                )
            else:
                value = None
        elif value == '-':
            value = None

        hero['appearance'][key] = value

    # Work
    for i, el in enumerate(work_els):
        key = work_keys[i]
        value = el.find_all('td')[1].text.strip()
        if value == '-':
            value = None
        hero['work'][key] = value

    # Connections
    for i, key in enumerate(connections_keys):
        key = connections_keys[i]
        value = connections_els[i * 2 + 1].find('td').text.strip()
        if key == 'teams' and value == 'No team connections added yet.':
            value = None
        if value == '-':
            value = None
        hero['connections'][key] = value

    # Strength level
    sl = soup.find_all('h4')[1].text
    sl = re.sub("([\(\[]).*?([\)\]])", "", sl).strip()
    if 'tons' in sl or 'kg' in sl:
        hero['strength_level']['value'], hero['strength_level']['unit'] = sl.split(' ')
    else:
        hero['strength_level']['value'] = sl

    # Powers
    url =  url + '/powers/'
    response = urllib.request.urlopen(url)
    data = response.read()
    soup2 = BeautifulSoup(data, 'html.parser')

    for row in soup2.find_all('div', class_='cblock back_blue'):
        for deeper in row.find_all('li'):
            hero['powers'].append(deeper.text.strip())

    # Retrieve image (resize 80px width)
    urllib.request.urlretrieve('https://www.superherodb.com' + image, image_path)
    img = Image.open(image_path)
    width = 160
    ratio = width / float(img.size[0])
    height = int(img.size[1] * ratio)
    img = img.resize((width, height), Image.ANTIALIAS)
    img.save(image_path, "JPEG", quality=75)

    hereos[id] = hero
    print(id, hero['name'])

with open(output, 'w') as fp:
    json.dump(hereos, fp, )
