/* jshint esversion: 6 */

var app;
var superhereos = {};

// Dom7
var $$ = Dom7;

function initApp() {
    // Framework7 App main instance
    app  = new Framework7({
      root: '#app',
      id: 'fr.febvre.superhero',
      name: 'Superhero',
      theme: 'auto',
      // App root data
      data: function () {
        return {};
      },
      // App root methods
      methods: {
        showNotification: function(title, subtitle, text) {
            var date = new Date();
            var notif = app.notification.create({
                icon: '<i class="icon material-icons">update</i>',
                title: title,
                titleRightText: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(undefined, {hour12: false}),
                subtitle: subtitle,
                text: text,
                closeOnClick: true
            });

            notif.open();
        },
      },
      // App routes
      routes: routes,
      // Enable panel left visibility breakpoint
      panel: {
        leftBreakpoint: 960,
      },
    });

    // Create main view and panel right view
    app.views.create('.view-main', {
        url: '/',
        main: true,
        pushState: true,
        pushStateRoot: location.href.split(location.host)[1].split('#!')[0]
    });
}


function initData() {
    var req = new Request('data/superhereos.json');

    fetch(req)
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {
            superhereos = data;
            initApp();
        });
}


function start() {
    // Service Worker registration
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('sw.js').then(registration => {

            // updatefound is fired if sw.js changes
            registration.onupdatefound = function() {
                // The updatefound event implies that registration.installing is set; see
                // https://w3c.github.io/ServiceWorker/#service-worker-registration-updatefound-event
                var installingWorker = registration.installing;

                installingWorker.onstatechange = function() {
                    if (installingWorker.state == 'installed' && navigator.serviceWorker.controller) {
                        // At this point, the old content will have been purged and the fresh content will
                        // have been added to the cache.
                        // It's the perfect time to display a message in the page's interface.
                        app.methods.showNotification(
                            'Update available',
                            'An update of the application is available',
                            'Please refresh (pull the page down)'
                        );
                    }
                };
            };
        }).catch(registrationError => {
            console.log('SW registration failed: ', registrationError);
        });
    }

    initData();
}

start();
