routes = [
    {
        path: '/',
        componentUrl: './pages/home.html'
    },
    {
        path: '/character/:id/',
        componentUrl: './pages/character.html',
    },
    {
        path: '/about/',
        templateUrl: './pages/about.html'
    },
    // Default route (404 page). MUST BE THE LAST
    {
        path: '(.*)',
        url: './pages/404.html',
    }
];
